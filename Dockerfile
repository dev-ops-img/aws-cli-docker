ARG AWS_CLI_VERSION
ARG DOCKER_VERSION

FROM amazon/aws-cli:${AWS_CLI_VERSION}

RUN amazon-linux-extras install docker=${DOCKER_VERSION}

ENTRYPOINT []